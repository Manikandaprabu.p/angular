import { Component } from '@angular/core';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent {
   //Data Buinding
  //  name ="prabu";
  // fun(){
  //   return this.name
  // }

   //event buinding
  // result=""
  // onSubmit(event:any){
  // this.result=(<HTMLInputElement>event.target).value
  // }

  //TwoWay Buinding
      // isEnter = "Mani"
      
      // ngIf
      // users = false;

      // ngFor
      // letters =[{name:"Kumar", age:23},
      //             {name:"Kani", age:34}]

      // ngSwitch
      // user =[{name:"Kumar", age:23,gender:"male"},
      // {name:"Kani", age:34, gender:"male"}]
     num=" "
}
