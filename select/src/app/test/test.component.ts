import { Component,Input,Output,EventEmitter
} from '@angular/core';

@Component({
  selector: 'app-test',
  // template: `<h1>test {{name}}" <button> (click)="sendDataToParent()</h1>`,
  templateUrl:'./test.Component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent {
   @Input('aliasName')name:string
   @Output() sendData:EventEmitter<any> = new EventEmitter<any>();

   sendDataToParent(){
    this.sendData.emit('Testing the transfar')
   }
}
